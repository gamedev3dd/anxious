using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using UnityEngine;

public class PlayerTriggerWithITC : MonoBehaviour
{
    
    private void OnTriggerEnter(Collider other)
    {
        
        ItemTypeComponent itc = other.GetComponent<ItemTypeComponent>();

       
        var inventory = GetComponent<Inventory>();
    
        var health = GetComponent<PlayerManager>();
        var healthNum = GetComponent<PlayerManager>();
      

       if(itc !=null)
       {
           switch (itc.Type)
           {
               case ItemType.Medkit:
                   if(health != null)
                       health.health = health.health + 30;
                      healthNum = health;
                   break;
               case ItemType.Syringe:
                   if(health != null)
                       health.health = health.health + 10;
                   healthNum = health;

                   break;
           }
           Destroy(other.gameObject,0);
       }
        
    }
}
