using System;
using UnityEngine;


    public class ItemTriggerWithPlayerHandler : MonoBehaviour
    {
        protected virtual void OnTriggerEnter(Collider other)
        {
            //Get the Inventory component from the player
                var inventory = other.GetComponent<Inventory>();
                //Add the collected item's tag name to the inventory
                inventory.AddItem(gameObject.tag,1);
        
                //Destroy itself
                Destroy(gameObject);
            }
        }

     

