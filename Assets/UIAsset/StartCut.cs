using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class StartCut : MonoBehaviour
{
    public void CutScene()
    {
        SceneManager.LoadScene("CutScene");
    }
}
